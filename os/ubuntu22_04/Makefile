# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

cargo_bin ?= $(HOME)/.cargo/bin
eos ?= $(this_dir)/../../eos/eos.py

.PHONY: all
all: dotfiles managers terminal

.PHONY: dotfiles
dotfiles:
	$(at)$(MAKE) -C ../../ dotfiles

.PHONY: managers
managers:
	$(at)$(eos) \
		--distro-id ubuntu \
		--distro-version 22.04 \
		--install \
			curl \
			go \
			pip
	$(at)$(eos) \
	 	--distro-id ubuntu \
		--distro-version 22.04 \
		--install \
			cargo \
			pip:distro \
			pipx

.PHONY: terminal
terminal:
	$(at)PATH=$(cargo_bin):$(PATH) $(eos) \
		--install \
			alacritty-deps \
			bat \
			bottom \
			delta \
			dust \
			exa \
			fd-find \
			gitg \
			lazygit \
			mccabe \
			neovim-deps \
			nodejs \
			procs \
			pycodestyle \
			pydocstyle \
			pyflakes \
			python-lsp-server \
			ripgrep \
			rope \
			tldr \
			tmux \
			zsh \
			zsh-syntax-highlighting
	$(at)PATH=$(cargo_bin):$(PATH) $(eos) \
		--install \
			alacritty \
			neovim \
			tree-sitter-cli
