#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

script_dir=$(cd $(dirname $0) && pwd)

source $script_dir/declare_flags.sh

variables["-t"]="test"
variables["--test"]="test"

source $script_dir/prepare_flags.sh

if [ ! -z $test ]; then
    printf '%s\n' \
        "You used the -t, --test, -t=<value>, or --test=<value> flag."
else
    printf '%s\n' \
        "You didn't use the -t, --test, -t=<value>, or --test=<value> flag.\n"
fi
