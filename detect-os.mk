# Copyright (c) 2023 Dominic Adam Walters

at ?= @
dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

ifeq ($(wildcard /etc/os-release),)
$(error /etc/os-release doesn't exist)
endif

os_name ?= $(shell \
	awk -F= '$$1=="ID" { print $$2 ;}' /etc/os-release)
os_version ?= $(shell \
	awk -F= '$$1=="VERSION_ID" { print $$2 ;}' /etc/os-release)