# Copyright (c) 2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: help
help:
	$(at)printf "%s\n" \
		"Usage:" \
		"  make          : Alias for \`make help\`." \
		"  make dotfiles : Install the contents of the \`dotfiles\` directory." \
		"  make help     : Print this message." \
		"  make update   : Update all submodules."

.PHONY: dotfiles
dotfiles:
	$(at)find $(this_dir)/dotfiles \
		-mindepth 1 -maxdepth 1 \
		-name ".*" -type f \
		-not -name ".gitmodules" \
		-not -name ".gitignore" \
		-exec cp -f {} $(HOME)/. \;
	$(at)find $(this_dir)/dotfiles \
		-mindepth 1 -maxdepth 1 \
		-name ".*" -type d \
		-not -name ".git" \
		-exec cp -fr {} $(HOME)/. \;

.PHONY: update
update:
	git submodule update --init --recursive
