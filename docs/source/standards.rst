.. MIT License

.. Copyright (c) 2023 Dominic Adam Walters

.. Permission is hereby granted, free of charge, to any person obtaining a copy
.. of this software and associated documentation files (the "Software"), to deal
.. in the Software without restriction, including without limitation the rights
.. to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
.. copies of the Software, and to permit persons to whom the Software is
.. furnished to do so, subject to the following conditions:
.. 
.. The above copyright notice and this permission notice shall be included in all
.. copies or substantial portions of the Software.
.. 
.. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
.. IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
.. FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
.. AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
.. LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
.. OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
.. SOFTWARE.

.. _Standards:

Standards
=========

.. _RFC 2119:

RFC 2119: Key words for use in RFCs to Indicate Requirement Levels
------------------------------------------------------------------

Useful standard that defines words commonly used in the writing of
requirements.

* `RFC 2119 Specification <https://www.rfc-editor.org/rfc/rfc2119>`_
* `RFC 8174 Specification <https://www.rfc-editor.org/rfc/rfc8174>`_

  * Update to RFC 2119.

.. _Semantic Versioning:

Semantic Versioning
-------------------

Standard defining a rigorous way to define versions for anything that might
need a version.

* `Semantic Versioning Specification <https://semver.org/#summary>`_

.. _Conventional Commits:

Conventional Commits
--------------------

Standard defining a semi-rigorous way to write commit messages.

* `Conventional Commits Specification <https://www.conventionalcommits.org/en/v1.0.0>`_
* `Suggested Commit Titles <https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum>`_
