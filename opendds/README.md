# OpenDDS Help

## Hostnames

OpenDDS domain registration requires the computer its being used on to have a
`hostname` that doesn't resolve to `localhost`.

To check if your computer is in a good state, run:

```bash
    ping $(hostname)
```

If this command returns a normal ping output, you're good. If it doesn't, you
need to add a hostname to your `/etc/hosts` file, and set it using `hostname`:

```bash
    name="newname"
    ip="192.168.7.1"
    hostname $name
    printf "$ip $name" | sudo tee -a /etc/hosts
```

`ping $(hostname)` should now act as expected.
