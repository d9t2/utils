# Copyright (c) 2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: default
default: install

include ../../detect-os.mk
include ../../commands.mk

$(this_dir)/delta.installed:
	$(at)# Check not already installed, then install
	$(at)if command -v delta &> /dev/null; then \
		echo "'delta' already installed."; \
	elif [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) git-delta; \
	else \
		echo "Operating system '$(os_name)' not supported by 'delta' installer"; \
		echo "Attempting cargo build..."; \
		$(cargo_install) git-delta; \
	fi
	$(at)touch $@

.PHONY: install
install: $(this_dir)/delta.installed

.PHONY: clean
clean:
	$(at)rm -f $(this_dir)/delta.installed
