# Copyright (c) 2022-2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: default
default: install

include $(this_dir)/../../detect-os.mk
include $(this_dir)/../../commands.mk

$(this_dir)/nvim.installed:
	$(at)# Check not already installed, then install
	$(at)if command -v nvim &> /dev/null; then \
		echo "'nvim' already installed."; \
	elif [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) nvim; \
	else \
		echo "OS has old version of 'nvim'."; \
		echo "Attempting build from sources..."; \
		$(MAKE) -C $(this_dir) from-sources; \
	fi
	$(at)touch $@

.PHONY: install
install: $(this_dir)/nvim.installed

.PHONY: from-sources
from-sources:
	$(at)# Get dependencies
	$(at)if [ "$(os_name)" = "centos" ]; then \
		$(sudo) $(yum_install) \
			ninja-build \
			libtool \
			autoconf \
			automake \
			cmake \
			gcc \
			gcc-c++ \
			make \
			pkgconfig \
			unzip \
			patch \
			gettext \
			curl; \
	elif [ "$(os_name)" = "ubuntu" ]; then \
		$(sudo) $(apt_install) \
			ninja-build \
			gettext \
			libtool \
			libtool-bin \
			autoconf \
			automake \
			cmake \
			g++ \
			pkg-config \
			unzip \
			curl \
			doxygen; \
	else \
		echo "Operating system '$(os_name)' not supported by source 'nvim' installer"; \
		exit 1; \
	fi
	$(at)# Get the source
	$(at)git clone https://github.com/neovim/neovim.git $(this_dir)/.
	$(at)# Build, and install
	$(at)$(MAKE) -C $(this_dir)/neovim CMAKE_BUILD_TYPE=Release
	$(at)$(sudo) $(MAKE) -C $(this_dir)/neovim install
	$(at)# Config dependencies
	$(at)## Language servers
	$(at)$(pip_install) \
		rope \
		pyflakes \
		mccabe \
		pycodestyle \
		pydocstyle
	$(at)$(pipx_install) python-lsp-server
	$(at)## Telescope
	$(at)$(cargo_install) ripgrep fd-find
	$(at)## Treesitter
	$(at)if [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) nodejs; \
	elif [ "$(os_name)" = "centos" ]; then \
		curl -sL https://rpm.nodesource.com/setup_14.x | sudo -E bash -; \
		$(sudo) $(yum_install) nodejs; \
	elif [ "$(os_name)" = "ubuntu" ]; then \
		$(sudo) $(apt_install) nodejs; \
	else \
		echo "Operating system '$(os_name)' not supported by 'nvim' installer"; \
		exit 1; \
	fi
	$(at)$(cargo_install) tree-sitter-cli

.PHONY: clean
clean:
	$(at)rm -f $(this_dir)/nvim.installed
