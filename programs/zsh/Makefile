# Copyright (c) 2022-2023 Dominic Adam Walters

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: default
default: install

include ../../detect-os.mk
include ../../commands.mk

$(this_dir)/zsh.installed:
	$(at)# Check not already installed, then install
	$(at)if command -v zsh &> /dev/null; then \
		echo "'zsh' already installed."; \
	elif [ "$(os_name)" = "arch" ]; then \
		$(sudo) $(pacman_install) \
			zsh \
			zsh-syntax-highlighting; \
	elif [ "$(os_name)" = "ubuntu" ]; then \
		$(sudo) $(apt_install) \
			zsh \
			zsh-syntax-highlighting; \
	elif [ "$(os_name)" = "centos" ]; then \
		$(sudo) $(yum_install) zsh; \
	else \
		echo "Operating system '$(os_name)' not supported by 'zsh' installer"; \
		exit 1; \
	fi
	$(at)touch $@

.PHONY: install
install: $(this_dir)/zsh.installed

.PHONY: clean
clean:
	$(at)rm -f $(this_dir)/zsh.installed
