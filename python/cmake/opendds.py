#!/usr/bin/env python3

# Copyright (c) 2021 Dominic Adam Walters

import argparse
import jinja2
import os

from copyright import generate_copyright_message
import utils.file
from utils.string import camel_case_to_snake_case

BUILD_TEMPLATE = """#!/usr/bin/env bash

# {{ copyright }}

script_dir=$(cd $(dirname $0) && pwd)

# Arguments
source $D9T2_UTILS_ROOT_DIR/scripting/flags/declare_flags.sh
variables["-c"]="clean"
variables["--clean"]="clean"
variables["-p"]="platform"
variables["--platform"]="platform"
source $D9T2_UTILS_ROOT_DIR/scripting/flags/prepare_flags.sh
# # Defaults
[ -z $clean ] && clean=0
if [ -z $platform ]; then
    platform="centos7"
    platform_was_set=0
else
    platform_was_set=1
fi

# OpenDDS Environment
source $DDS_ROOT/setenv.sh
tao_flags="--idl-version 4 --unknown-annotations ignore -Cw"
idl_name={{ idl.camel }}

# Function Definitions
function tao_idl_bin {
    tao_idl=$DDS_ROOT/ACE_wrappers/bin/tao_idl
    if command -v $tao_idl &> /dev/null; then
        $tao_idl $@
    else
        echo "$DDS_ROOT/bin/tao_idl not found"
        exit 1
    fi
}
function opendds_idl_bin {
    opendds_idl=$DDS_ROOT/bin/opendds_idl
    if command -v $opendds_idl &> /dev/null; then
        $opendds_idl $@
    else
        echo "$DDS_ROOT/bin/opendds_idl not found"
        exit 1
    fi
}
function cmake_bin {
    if command -v cmake &> /dev/null; then
        cmake $@
    elif command -v cmake3 &> /dev/null; then
        cmake3 $@
    else
        echo "CMake not found"
        exit 1
    fi
}

# Toolchain files
toolchain_file=$D9T2_UTILS_ROOT_DIR/cmake/toolchains/$platform.cmake
[ ! -f $toolchain_file ] && \
    echo "The toolchain file $toolchain_file does not exist." && \
    exit 1

# Make artifact directories if they don't exist
top_build_dir=$script_dir/gen
build_dir=$top_build_dir/$platform
mkdir -p $build_dir

# Clean the library if clean was requested and the library has been built
if [ $clean -eq 1 ]; then
    if [ $platform_was_set -eq 0 ]; then
        rm -rf $top_build_dir
    else
        rm -rf $build_dir
    fi
    rm -rf $script_dir/src
    rm -rf $script_dir/include
    rm -rf $script_dir/compile_commands.json
    exit 0
fi

# Generate source code
mkdir -p $script_dir/src
cd $script_dir/src
if { [ ! -f $build_dir/$idl_name.idl ] || \
        ! cmp $script_dir/$idl_name.idl $build_dir/$idl_name.idl --silent; }
then
    tao_idl_bin $tao_flags $script_dir/$idl_name.idl
    opendds_idl_bin $script_dir/$idl_name.idl
    tao_idl_bin $tao_flags -I$DDS_ROOT -I$script_dir \
        $script_dir/src/${idl_name}TypeSupport.idl
    cp $script_dir/$idl_name.idl $build_dir/.
fi

# Assemble include directory
mkdir -p $script_dir/include
mv $script_dir/src/*.inl $script_dir/include/. &> /dev/null
mv $script_dir/src/*.h $script_dir/include/. &> /dev/null

# CMake
cd $build_dir
cmake3 $script_dir -DCMAKE_TOOLCHAIN_FILE=$toolchain_file
cmake3 --build $build_dir

# compile_commands.json
cp -fl $build_dir/compile_commands.json $script_dir/.

# Assemble lib directory
ln -fs $script_dir/build/*.so* $script_dir/lib/.
\n"""


CMAKELISTS_TEMPLATE = """# {{ copyright }}
cmake_minimum_required(VERSION 3.16)
set(PRJ_MAJOR_VERSION 1)
set(PRJ_MINOR_VERSION 0)
set(PRJ_PATCH_VERSION 0)
project({{ idl.snake }}
  VERSION ${PRJ_MAJOR_VERSION}.${PRJ_MINOR_VERSION}.${PRJ_PATCH_VERSION}
  LANGUAGES CXX
  DESCRIPTION "Shared Library for {{ idl.camel }}"
)
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY lib)

# Shared Library
add_library({{ idl.snake }} SHARED
  ${CMAKE_SOURCE_DIR}/src/{{ idl.camel }}C.cpp
  ${CMAKE_SOURCE_DIR}/src/{{ idl.camel }}S.cpp
  ${CMAKE_SOURCE_DIR}/src/{{ idl.camel }}TypeSupportC.cpp
  ${CMAKE_SOURCE_DIR}/src/{{ idl.camel }}TypeSupportImpl.cpp
  ${CMAKE_SOURCE_DIR}/src/{{ idl.camel }}TypeSupportS.cpp
)
target_include_directories({{ idl.snake }} PUBLIC
  $ENV{ACE_ROOT}
  $ENV{TAO_ROOT}
  $ENV{DDS_ROOT}
  ${CMAKE_SOURCE_DIR}/include
)
set_target_properties({{ idl.snake }}
  PROPERTIES
    VERSION ${PROJECT_VERSION}
    SOVERSION ${PRJ_MAJOR_VERSION}
    POSITION_INDEPENDENT_CODE ON
)
\n"""


GITIGNORE_TEMPLATE = """
build/
include/
lib/
src/
compile_commands.json
\n"""


class IDL:
    """
    Class representing the multiple ways of representing the IDL's name

    Attributes
    ----------
    camel : str
        The camelcase representation of the IDL Name
    snake : str
        The snakecase representation of the IDL Name
    """
    def __init__(self, idl):
        self.camel = idl
        self.snake = camel_case_to_snake_case(idl)


class CMakeProject:
    """
    Class representing a CMake project to build shared libraries for OpenDDS
    generated topic source code

    Attributes
    ----------
    idl : IDL
        An IDL object
    location : str
        The directory that should contain the new CMake Project

    Methods
    -------
    generate():
        Generate the build.sh, clean.sh, and CMakeLists.txt files
    generate_build_content():
        Return a jinja2 string representing the contents of the build.sh file
        for the CMake Project
    generate_build_file():
        Write generate_build_content() to {self.location}/build.sh
    generate_cmakelist_content():
        Return a jinja2 string representing the contents of the CMakeLists.txt
        file for the CMake Project
    generate_cmakelist_file():
        Write generate_cmakelist_content() to {self.location}/CMakeLists.txt
    generate_gitignore_content():
        Return a jinja2 string representing the contents of the .gitignore
        file for the CMake Project
    generate_gitignore_file():
        Write generate_gitignore_content() to {self.location}/.gitignore
    """
    def __init__(self, idl, location):
        self.idl = IDL(idl)
        self.location = location

    def generate(self):
        """
        Generate the build.sh, clean.sh, and CMakeLists.txt files
        """
        self.generate_build_file()
        self.generate_cmakelist_file()
        self.generate_gitignore_file()

    def generate_build_content(self):
        """
        Return a jinja2 string representing the contents of the build.sh file
        for the CMake Project

        Returns
        -------
        str
            The jinja2 string
        """
        tm = jinja2.Template(BUILD_TEMPLATE)
        return tm.render(copyright=generate_copyright_message(), idl=self.idl)

    def generate_build_file(self):
        """
        Write generate_build_content() to {self.location}/build.sh
        """
        file_name = f"{self.location}/build.sh"
        with open(file_name, "w") as f:
            f.write(self.generate_build_content())
        utils.file.make_executable(file_name)

    def generate_cmakelist_content(self):
        """
        Return a jinja2 string representing the contents of the CMakeLists.txt
        file for the CMake Project

        Returns
        -------
        str
            The jinja2 string
        """
        tm = jinja2.Template(CMAKELISTS_TEMPLATE)
        return tm.render(copyright=generate_copyright_message(), idl=self.idl)

    def generate_cmakelist_file(self):
        """
        Write generate_cmakelist_content() to {self.location}/CMakeLists.txt
        """
        file_name = f"{self.location}/CMakeLists.txt"
        with open(file_name, "w") as f:
            f.write(self.generate_cmakelist_content())

    def generate_gitignore_content(self):
        """
        Return a jinja2 string representing the contents of the .gitignore
        file for the CMake Project

        Returns
        -------
        str
            The jinja2 string
        """
        tm = jinja2.Template(GITIGNORE_TEMPLATE)
        return tm.render(copyright=generate_copyright_message(), idl=self.idl)

    def generate_gitignore_file(self):
        """
        Write generate_gitignore_content() to {self.location}/.gitignore
        """
        file_name = f"{self.location}/.gitignore"
        with open(file_name, "w") as f:
            f.write(self.generate_gitignore_content())


def main(args):
    """
    Create a CMake Project to build a shared library containing the source code
    of a specific Topic IDL

    Parameters
    ----------
    args : Namespace
        A Namespace created by the argparse module. This must contain entries
        for `dir` and `idl`

    Exceptions
    ----------
    ValueError
        No IDL name was specified
    """
    if args.idl is None:
        raise ValueError("No IDL name specified")
    project = CMakeProject(args.idl, args.dir)
    project.generate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=("Create OpenDDS Topic CMake "
                                                  "Projects"))
    parser.add_argument("-d", "--dir", type=str, default=os.getcwd(),
                        help=("An existing directory to create the CMake "
                              "project in. This directory must contain the "
                              "requested IDL file. This defaults to the "
                              "current working directory."))
    parser.add_argument("-i", "--idl", type=str, default=None,
                        help="The name of the .idl file to use.")
    args = parser.parse_args()
    main(args)
