#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# Environment
script_dir=$(cd $(dirname $0) && pwd)
source ../../settings.sh

# Arguments
source $D9T2_UTILS_ROOT_DIR/scripting/flags/declare_flags.sh
variables["-p"]="platform"
variables["--platform"]="platform"
variables["-v"]="version"
variables["--version"]="version"
source $D9T2_UTILS_ROOT_DIR/scripting/flags/prepare_flags.sh
# # Defaults
if [ -z $platform ]; then
    directory=$script_dir/builds/host
else
    directory=$script_dir/builds/$platform
fi
if [ -z $version ]; then
    version=6.2.1
fi

# Get gmp release if we haven't got it
mkdir -p $script_dir/builds
if [ ! -d $script_dir/builds/src ]; then
    cd $script_dir/builds
    wget https://gmplib.org/download/gmp/gmp-$version.tar.xz
    tar --extract -f gmp-$version.tar.xz
    mv gmp-$version src
fi

# Validate $platform
if [ ! -z $platform ]; then
    $D9T2_UTILS_ROOT_DIR/crosscompiling/check_crosscompile_platform.sh \
        --platform $platform
fi

# Build
# # Make gmp clone for the platform
if [ ! -d $directory ]; then
    cp -r $script_dir/builds/src $directory
fi
prefix=$directory/build
mkdir -p $prefix
# # Configure
cd $directory
if [ -z $platform ]; then
    # # # Compile
    path=$PATH
    ./configure --prefix=$prefix
else
    # # # Cross Compile
    source $D9T2_UTILS_ROOT_DIR/crosscompiling/toolchains/$platform/setenv.sh
    path=$PATH:$CROSS_COMPILER_ROOT/bin
    PATH=$path \
        CFLAGS="-I$CROSS_COMPILER_ROOT/include" \
        LDFLAGS="-L$CROSS_COMPILER_ROOT/lib" \
            ./configure \
                --build=$($directory/config.guess) --host=$CROSS_COMPILER \
                --prefix=$prefix
fi
# # Make
if PATH=$path make; then
    PATH=$path make install
else
    echo "Build failed"
    exit -1
fi
