#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# Environment
script_dir=$(cd $(dirname $0) && pwd)
source ../settings.sh

# Arguments
source $D9T2_UTILS_ROOT_DIR/scripting/flags/declare_flags.sh
variables["-p"]="platform"
variables["--platform"]="platform"
source $D9T2_UTILS_ROOT_DIR/scripting/flags/prepare_flags.sh
# # Defaults
if [ -z $platform ]; then
    echo "This script requires a --platform argument"
    exit -1
fi

# Action
# # Check whether $platform has a directory in crosscompiling/toolchains
if [ ! -d $D9T2_UTILS_ROOT_DIR/crosscompiling/toolchains/$platform ]; then
    echo "$platform is not a supported crosscompiling toolchain"
    exit -1
fi
