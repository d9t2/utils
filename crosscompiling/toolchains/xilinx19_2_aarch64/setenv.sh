#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# Check if script is sourced
if ! (return 0 &> /dev/null); then
    echo "This script must be sourced"
    exit 1
fi

export CROSS_COMPILER_ROOT=/opt/Xilinx/Vitis/2019.2/gnu/aarch64/lin/aarch64-linux
export CROSS_COMPILER=aarch64-linux-gnu
