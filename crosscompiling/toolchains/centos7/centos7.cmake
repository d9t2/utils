# Copyright (c) 2021 Dominic Adam Walters
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_VERSION 3.10.0)

# Compiler
set(PLATFORM centos7)
set(CMAKE_FIND_ROOT_PATH
  /opt/rh/devtoolset-8/root/usr
)
set(CMAKE_C_COMPILER gcc)
set(CMAKE_CXX_COMPILER g++)

# Headers and Libraries
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM ONLY)
