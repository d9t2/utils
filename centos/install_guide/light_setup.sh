#!/usr/bin/env bash

# Disable Mouse Acceleration
printf '%s\n%s\n%s\n%s\n%s\n%s\n%s\n' \
    "Section \"InputClass\"" \
    "	Identifier \"My Mouse\"" \
    "	MatchIsPointer \"yes\"" \
    "	Option \"AccelerationNumerator\" \"1\"" \
    "	Option \"AccelerationDenominator\" \"1\"" \
    "	Option \"AccelerationThreshold\" \"0\"" \
    "EndSection" | sudo tee -a /etc/X11/xorg.conf.d/50-mouse-acceleration.conf

# Git
sudo yum -y install git git-gui
# GParted for disk management
sudo yum -y install gparted

# Epel
sudo yum -y install epel-release centos-release-scl
sudo yum -y install devtoolset-8
printf "source /opt/rh/devtoolset-8/enable\n" >> $HOME/.bashrc

# Xilinx
xilinx=/opt/Xilinx
sudo mkdir $xilinx
sudo chown -R $USER: $xilinx
mkdir $xilinx/SDK $xilinx/Vitis $xilinx/Vitis_HLS $xilinx/Vivado
mkdir $xilinx/ZynqReleases $xilinx/git $xilinx/installs
git clone https://github.com/xilinx/linux-xlnx.git $xilinx/git/linux-xlnx
git clone https://github.com/xilinx/u-boot-xlnx.git $xilinx/git/u-boot-xlnx

# OpenCPI
opencpi_loc=/home/$USER/opencpi
git clone https://gitlab.com/opencpi/opencpi.git $opencpi_loc
cd $opencpi_loc
git checkout release-2.0.1
./scripts/install-opencpi.sh
printf "source /home/$USER/opencpi/cdk/opencpi-setup.sh -r\n" >> $HOME/.bashrc

# Reminders to the user
xilinx_version=2019.2
echo ""
echo "light_setup.sh completed!"
echo ""
echo "Reminder: Setup your git config:"
echo "  git config --global user.email <email>"
echo "  git config --global user.name  <name>"
echo ""
echo "Reminder: Get ZynqRelease tarballs and put them in $xilinx/ZynqReleases:"
echo "  cd Downloads"
echo "  mkdir $xilinx/ZynqReleases/$xilinx_version"
echo "  cp $xilinx_version-zcu102-release.tar.xz \\"
echo "      $xilinx/ZynqReleases/$xilinx_version/."
echo "  cp $xilinx_version-zed-release.tar.xz \\"
echo "      $xilinx/ZynqReleases/$xilinx_version/."
