# TODO

## delta

* Add language support for `vhdl`.

  * https://dandavison.github.io/delta/supported-languages-and-themes.html
  * Uses sublime syntax.

## nvim

* Add language server for `vhdl`.

  * https://github.com/ghdl/ghdl-language-server
  * https://ghdl.github.io/ghdl-cosim/
  * https://github.com/ghdl/ghdl
  * https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md

* `gitsigns` appears to be broken?
* Look into more plugins and configs:

  * `twilight`
  * `noice`
  * Session manager?
  * Tabs?
  * Actually use telescope properly?

## lazygit

* Add installer.
