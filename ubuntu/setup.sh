#!/usr/bin/env bash

# Copyright (c) 2021 Dominic Adam Walters

# Script location and name
me=$(basename $0)
script_dir=$(cd $(dirname $0) && pwd)
cd $script_dir

# Get necessary packages
sudo apt update && sudo apt -y upgrade
# # Git
sudo apt -y install git git-gui
# # GParted for disk management
sudo apt -y install gparted
# # Microsoft Edge for browsing
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > \
    /tmp/microsoft.gpg
sudo install -o root -g root -m 644 /tmp/microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm /tmp/microsoft.gpg
sudo apt update && sudo apt -y install microsoft-edge-dev
# # Ability to mount lvm partitions
sudo apt -y install lvm2

# Custom Vim install
cd ../
git submodule update --init --recursive
cd programs/vim
./setup.sh
# # Clean Up
rm -rf vim/
cd $script_dir

# Xilinx
xilinx=/opt/Xilinx
sudo mkdir $xilinx
sudo chown -R $USER: $xilinx
mkdir $xilinx/SDK $xilinx/Vitis $xilinx/Vitis_HLS $xilinx/Vivado
mkdir $xilinx/ZynqReleases $xilinx/git
git clone https://github.com/xilinx/linux-xlnx.git $xilinx/git/linux-xlnx
git clone https://github.com/xilinx/u-boot-xlnx.git $xilinx/git/u-boot-xlnx

# OpenCPI
opencpi_loc=/home/$USER/opencpi
git clone https://gitlab.com/opencpi/opencpi.git $opencpi_loc
cd $opencpi_loc
./scripts/install-packages.sh
./scripts/install-prerequisites.sh

# Reminders to the user
echo ""
echo "setup.sh completed!"
echo ""
echo "Reminder: Setup your git config:"
echo "  \`git config --global user.email <email>\`"
echo "  \`git config --global user.name  <name>\`"
echo ""
echo "Reminder: Get ZynqRelease tarballs and put them in $xilinx/ZynqReleases:"
echo "  \`cd Downloads\`"
echo "  \`mkdir $xilinx/ZynqReleases/2018.3\`"
echo "  \`cp 2018.3-zcu102-release.tar.xz $xilinx/ZynqReleases/2018.3/.\`"
echo "  \`cp 2018.3-zed-release.tar.xz $xilinx/ZynqReleases/2018.3/.\`"
